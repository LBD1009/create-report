﻿namespace View
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableAdapterManager1 = new DTO.dsSVTableAdapters.TableAdapterManager();
            this.dsSV1 = new DTO.dsSV();
            this.sinhVienTableAdapter1 = new DTO.dsSVTableAdapters.SinhVienTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dsSV1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.SinhVienTableAdapter = this.sinhVienTableAdapter1;
            this.tableAdapterManager1.UpdateOrder = DTO.dsSVTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // dsSV1
            // 
            this.dsSV1.DataSetName = "dsSV";
            this.dsSV1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sinhVienTableAdapter1
            // 
            this.sinhVienTableAdapter1.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dsSV1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DTO.dsSVTableAdapters.TableAdapterManager tableAdapterManager1;
        private DTO.dsSV dsSV1;
        private DTO.dsSVTableAdapters.SinhVienTableAdapter sinhVienTableAdapter1;
    }
}

